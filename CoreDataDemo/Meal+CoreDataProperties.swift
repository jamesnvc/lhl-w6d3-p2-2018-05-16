//
//  Meal+CoreDataProperties.swift
//  CoreDataDemo
//
//  Created by James Cash on 16-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//
//

import Foundation
import CoreData


extension Meal {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Meal> {
        return NSFetchRequest<Meal>(entityName: "Meal")
    }

    @NSManaged public var time: NSDate?
    @NSManaged public var servings: Double
    @NSManaged public var eatenFood: Food?

}
