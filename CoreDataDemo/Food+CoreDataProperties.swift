//
//  Food+CoreDataProperties.swift
//  CoreDataDemo
//
//  Created by James Cash on 16-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//
//

import Foundation
import CoreData


extension Food {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Food> {
        return NSFetchRequest<Food>(entityName: "Food")
    }

    @NSManaged public var name: String?
    @NSManaged public var calories: NSNumber?
    @NSManaged public var meals: NSSet?

}

// MARK: Generated accessors for meals
extension Food {

    @objc(addMealsObject:)
    @NSManaged public func addToMeals(_ value: Meal)

    @objc(removeMealsObject:)
    @NSManaged public func removeFromMeals(_ value: Meal)

    @objc(addMeals:)
    @NSManaged public func addToMeals(_ values: NSSet)

    @objc(removeMeals:)
    @NSManaged public func removeFromMeals(_ values: NSSet)

}

extension Food {
    convenience init(context: NSManagedObjectContext, name: String, kcals: Double) {
        self.init(context: context)
        self.name = name
        self.calories = NSNumber(value: kcals)
    }
}
