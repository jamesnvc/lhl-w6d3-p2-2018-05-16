//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by James Cash on 16-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet var servingsTextField: UITextField!
    @IBOutlet var foodsTableView: UITableView!

    @IBOutlet var mealsTableView: UITableView!
    var foodsFetchController: NSFetchedResultsController<Food>!

    var mealsFetchController: NSFetchedResultsController<Meal>!

    override func viewDidLoad() {
        super.viewDidLoad()

        let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            let foods = try moc.fetch(Food.fetchRequest())
            if foods.count == 0 {
                seedFoods()
            } else {
                print("Foods: \(foods)")
            }
        } catch let err {
            print("Failed to fetch: \(err.localizedDescription)")
        }

        let foodFetch: NSFetchRequest<Food> = Food.fetchRequest()
        foodFetch.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        foodsFetchController = NSFetchedResultsController(
            fetchRequest: foodFetch,
            managedObjectContext: moc,
            sectionNameKeyPath: nil,
            cacheName: nil)
        foodsFetchController.delegate = self
        do {
            try foodsFetchController.performFetch()
        } catch let err {
            print("Error creating food fetch:\(err.localizedDescription)")
        }

        let mealFetch: NSFetchRequest<Meal> = Meal.fetchRequest()
        mealFetch.sortDescriptors = [NSSortDescriptor(key: "time", ascending: false)]
        mealsFetchController = NSFetchedResultsController(
            fetchRequest: mealFetch,
            managedObjectContext: moc,
            sectionNameKeyPath: nil, // make method on Meal to get the day
            cacheName: "mealsCache")
        mealsFetchController.delegate = self
        do {
            try mealsFetchController.performFetch()
        } catch let err {
            print("error creating meal fetch: \(err.localizedDescription)")
        }
    }

    func seedFoods() {
        let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        Food(context: moc, name: "steamed hams", kcals: 500)
        Food(context: moc, name: "pho", kcals: 300)
        Food(context: moc, name: "coffee", kcals: 0)
        do { try moc.save() }
        catch let err { print("Error saving: \(err.localizedDescription)") }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addMeal(food: Food) {
        let servings = Double(servingsTextField.text ?? "0") ?? 0
        let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        let meal = Meal(context: moc)
        meal.servings = servings
        meal.eatenFood = food
        meal.time = NSDate()
        try! moc.save() // actually check this error
    }

}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == foodsFetchController {
            foodsTableView.beginUpdates()
        } else {
            mealsTableView.beginUpdates()
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == foodsFetchController {
            foodsTableView.endUpdates()
        } else {
            mealsTableView.endUpdates()
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        let tableView: UITableView
        if controller == foodsFetchController {
            tableView = foodsTableView
        } else {
            tableView = mealsTableView
        }

        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        default:
            print("something else \(type)")
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == foodsTableView {
            return foodsFetchController.fetchedObjects?.count ?? 0
        } else {
            return mealsFetchController.fetchedObjects?.count ?? 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == foodsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell")!
            let label = cell.viewWithTag(1) as! UILabel
            let food = foodsFetchController.fetchedObjects![indexPath.row]
            label.text = "\(food.name!): \(food.calories!)"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell")!
            let label = cell.viewWithTag(1) as! UILabel
            let meal = mealsFetchController.fetchedObjects![indexPath.row]
            label.text = "\(meal.eatenFood!.name!) x \(meal.servings)"
            return cell
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let food = foodsFetchController.fetchedObjects![indexPath.row]
        addMeal(food: food)
    }
}

extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let txt: NSString = textField.text! as NSString
        let searchQuery = txt.replacingCharacters(in: range, with: string)
        setSearch(query: searchQuery)
        return true
    }

    func setSearch(query: String) {

//        NSFetchedResultsController.deleteCache(withName: "foodsCache")
        if query.count == 0 {
            foodsFetchController.fetchRequest.predicate = nil
        } else {
            foodsFetchController.fetchRequest.predicate = NSPredicate(format: "name contains[cd] %@", query)
            // [cd] means do a *C*ase-insensitive & *D*iacritic-insensitive comparision
        }
        foodsFetchController.delegate = self
        try! foodsFetchController.performFetch()
        // looks like the controller doesn't update properly when changing the predicate, so we have to just reload data
        foodsTableView.reloadData()
    }
}
